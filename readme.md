# calenders 
If you need to mass download calenders a.k.a jewish holidays and lunar, and save them as json file, use this package.

## Install

```
$ npm install -g calenders
```

## Usage
The package allows you to download calenders, any number of years from a given year and month. the cli offers variety of calenders all using: 

```
$ calenders -h
    -m, --moonlight, Download moonlight calender
    -j, --jewish, download jewish holidays
    -i, --islamic, download islamic holidays
    -o, --output [directory]   The output [directory].
```

### Jewish
When using the jewish flag the downloader will download every jewish holiday between the given years.

### Islamic
When using the islamic flag the downloader will download every islamic holiday between the given years.
 
### Moonlight
When using the moonlight flag the downloader will download every lunar phase between the given years.
