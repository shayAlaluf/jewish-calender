const jewishCalendar = require('./jewish-holidays');
const islamicHolidays = require('./islamic-holidays');
const lunarCalendar = require('./moonlight');
const writeToFile = require('./file-handler');

const getCalendar = async opts => {
	if (opts.jewish) {
		return await jewishCalendar();
	} else if (opts.islamic) {
		return await islamicHolidays();
	} else if (opts.moonlight) {
		return await lunarCalendar();
	}
};

const app = async (opts, logger) => {
	const calendar = await getCalendar(opts);

	logger.info('saving file to path');

	try {
		await writeToFile(calendar, opts.output);
		logger.info(`Done! your calendar at ${opts.output}\\calendar.json`);
	} catch (e) {
		logger.error(`could not write to file: ${opts.output}\n try different path`);
	}
};

module.exports = app;
