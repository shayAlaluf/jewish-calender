const axios = require('axios');
const inquire = require('../inquires/yearAndMonth');
const {logger} = require('../logger');

const nameConverter = {
	"Lailat-ul-Miraj": "לילת אל-מעראג'",
	"Lailat-ul-Bara'at": "לילת אל-בהרהת",
	"1st Day of Ramadan": "רמאדן יום ראשון",
	"Lailat-ul-Qadr": "לילת אל-קדר",
	"Eid-ul-Fitr": "עיד אל-פיטר",
	"Hajj": "חג'",
	"Arafa": "יום ארפה",
	"Eid-ul-Adha": "חג הקורבן",
	"Ashura": "עשוראא'",
	"Mawlid al-Nabi": "מוולד א-נבי"
};

const getUrl = (mouth, year) => `http://api.aladhan.com/v1/gToHCalendar/${mouth}/${year}`;

const getMonthData = async (mouth, year) => (await axios.get(getUrl(mouth, year), {headers: {"retry-after": 120}})).data.data;

const downloadCalendar = async (initialYear, initialMonth, calendarNumOfYears) => {
	const calendar = [];
	for (let year = initialYear; year < initialYear + calendarNumOfYears; year++) {
		logger.info(`Downloading year ${year}...`);
		for (let month = initialMonth; month <= 12; month++) {
			try {
				const islamicMonth = await getMonthData(month, year);
				calendar.push(islamicMonth);
			} catch (error) {
				if(error.response.status != '429'){
					logger.error(error);
				}
			}
		}
	}

	return normalize(calendar);
};

const fixDate = (date) => {
	const dateArray = (date).split('-');
	const day = dateArray[1];
	dateArray[1] = dateArray[0];
	dateArray[0] = day;
	return new Date(dateArray.join('-'));
};

const normalize = calendarArray => {
	const calendar = {};
	calendarArray.forEach(islamicYear => {
		islamicYear.forEach(islamicMonth => {
			const holidays = islamicMonth.hijri.holidays;
			if (holidays.length) {
				const date = fixDate(islamicMonth.gregorian.date).toLocaleDateString();
				calendar[date] = holidays.map(holiday => nameConverter[holiday]);
			}
		})
	});

	return calendar;
};

const run = async () => {
	const userConfig = await inquire();
	return await downloadCalendar(userConfig['year'], userConfig['month'], userConfig['numOfYears']);
};

module.exports = run;
