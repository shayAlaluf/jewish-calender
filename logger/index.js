const {createLogger, format, transports} = require('winston');
const {combine, errors, printf} = format;
const errorConfig = require('./error-configuration');

const logger = createLogger({
	format: combine(
		errors({stack: true}),
		printf(info => `${info.timestamp} : ${JSON.stringify(info.message)}`)
	),
	transports: [
		new transports.Console({
			level: 'info',
			json: false,
			showLevel: false,
			consoleWarnLevels: ['warn', 'debug']
		})
	],
	exitOnError: false
});

module.exports = {
	logger: logger,
	errorsConfiguration: errorConfig
};
