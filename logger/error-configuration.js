const {transports} = require('winston');

module.exports = (logger, output) => logger.add(
	new transports.File({
		level: 'error',
		filename: `${output}\\errorLog.log`
	})
);
