const axios = require('axios');
const inquire = require('../inquires/year');
const {logger} = require('../logger');

const url = 'http://www.hebcal.com/hebcal/';

const getUrlData = async config => (await axios.get(url, {params: config})).data;

let getConfigMoon = year => ({
	v: 1,
	cfg: 'json',
	maj: 'on',
	min: 'on',
	mf: 'on',
	mod: 'on',
	city: "Tel Aviv",
	month: 'x',
	lg: 'sh',
	year: year,
});

const getYearlyCalendar = async year => {
	const configMoon = getConfigMoon(year);
	return await getUrlData(configMoon);
};

const downloadCalendar = async (initialYear, calendarNumOfYears) => {
	const calendar = [];
	for (let year = initialYear; year < initialYear + calendarNumOfYears; year++) {
		logger.info(`downloading year ${year}...`);
		const holidays = await getYearlyCalendar(year);
		calendar.push(holidays);
	}

	return normalize(calendar);
};

function normalize(array) {
	const calendar = {};
	array.forEach(yearlyHolidays => {
		yearlyHolidays.items.forEach(({hebrew, date, subcat}) => {
			const localeDate = (new Date(date)).toLocaleDateString();
			calendar[localeDate] = {name: hebrew, subcat};
		});
	});

	return calendar;
}

const run = async () => {
	const userConfig = await inquire();
	return await downloadCalendar(userConfig['year'], userConfig['numOfYears']);
};

module.exports = run;
