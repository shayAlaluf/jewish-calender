const year = require('./year');
const yearAndMonth = require('./yearAndMonth');

module.exports = {
	year,
	yearAndMonth
};
