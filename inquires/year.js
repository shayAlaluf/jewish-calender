const inquirer = require("inquirer");

const question = [
	{
		type: 'number',
		name: 'year',
		message: 'From what year you wish to start your calendar?',
	},
	{
		type: 'number',
		name: 'numOfYears',
		message: 'How many years you wish to display?'
	}
];

const inquire = async () => await inquirer.prompt(question);

module.exports = inquire;
