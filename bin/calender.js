#!/usr/bin/env node
const program = require('commander');
const {logger, errorsConfiguration} = require('../logger');

const app = require('../');

program
	.version('0.1.4')
	.option('-m, --moonlight', 'Download moonlight calendar')
	.option('-j, --jewish', 'download jewish holidays')
	.option('-i, --islamic', 'download islamic holidays')
	.option('-o, --output <type>', 'The output [directory].')
	.parse(process.argv);

if (!program.output) {
	console.error("No output directory were given!");
	process.exit();
}

errorsConfiguration(logger, program.output);

logger.info("Welcome to json Calendar.");
app(program.opts(), logger);
