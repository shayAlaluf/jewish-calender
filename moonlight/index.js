const axios = require('axios');
const inquire = require('../inquires/yearAndMonth');
const {logger} = require('../logger');

const url = 'https://www.icalendar37.net/lunar/api/';

const getLunarMonth = async config => (await axios.get(url, {params: config})).data;

let getConfigMoon = (year, month) => ({
	lang: 'en',
	month: month,
	year: year,
	size: 50,
});

const getMonthPhases = async (wantedYear, wantedMonth) => {
	const configMoon = getConfigMoon(wantedYear, wantedMonth);
	configMoon.LDZ = new Date(configMoon.year, configMoon.month - 1, 1) / 1000;
	const lunarMonth = await getLunarMonth(configMoon);
	const {phase, monthName, year, month} = lunarMonth;
	return {phase, monthName, year, month};
};

const downloadCalendar = async (initialYear, initialMonth, calendarNumOfYears) => {
	const lunarCalendar = [];
	for (let year = initialYear; year < initialYear + calendarNumOfYears; year++) {
		logger.info(`Downloading year ${year}...`);
		for (let month = initialMonth; month <= 12; month++) {
			const lunarPhase = await getMonthPhases(year, month);
			lunarCalendar.push(lunarPhase);
		}
	}

	return normalize(lunarCalendar);
};

const setCalendarValue = (calendar, path, value) => {
	const firstPathValue = path[0];

	if (path.length === 1) {
		calendar[firstPathValue] = value;
	}

	if (!calendar[firstPathValue]) {
		calendar[firstPathValue] = {};
	}

	path.shift();
	if (path.length) {
		setCalendarValue(calendar[firstPathValue], path, value)
	}
};

const normalize = calendarArray => {
	const calendar = {};
	calendarArray.forEach(lunarMonth => {
		for (let day in lunarMonth.phase) {
			setCalendarValue(calendar, [lunarMonth.year, lunarMonth.month, day], lunarMonth.phase[day]['lighting']);
		}
	});

	return calendar;
};

const run = async () => {
	const userConfig = await inquire();
	return await downloadCalendar(userConfig['year'], userConfig['month'], userConfig['numOfYears']);
};

module.exports = run;
