const fs = require('fs');

const noop = () => {};
const writeToFile = async (calendar, filePath) => fs.writeFile(`${filePath}\\calendar.json`, JSON.stringify(calendar), noop);

module.exports = writeToFile;
